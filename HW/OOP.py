class Rectangle:

    def __init__(self, length: int, width: int):
        if length == 0:
            raise Exception
        if width == 0:
            raise Exception
        self.length = length
        self.width = width


    def __add__(self, other):
        square_1 = self.length * self.width
        square_2 = other.length * other.width
        answer = Rectangle(square_1 + square_2)
        return answer


    def __sub__(self, other):
        square_1 = self.length * self.width
        square_2 = other.length * other.width
        if square_1 < square_2:
            square_1, square_2 = square_2, square_1
            answer = square_1 - square_2
            return answer


    def __eq__(self, other):
        square_1 = self.length * self.width
        square_2 = other.length * other.width
        return square_1 == square_2


    def __str__(self, other):
        return "Площадь первого прямоугольника - {}/n Площадь второго прямоугольника - {}".format(self.length * self.width, other.length * other.width)