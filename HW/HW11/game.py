import random

from player import User

percents = {1: 100, 2: 90, 3: 80, 4: 70, 5: 60, 6: 50, 7: 40, 8: 30, 9: 20}


def get_change(percent: int) -> bool:
    percent /= 10
    change = random.randint(1, 9)
    return 0 < change < percent


def check_power_attack(power: int) -> int:
    return power if get_change(percents.get(power)) else 0


def attack(user_one: User, user_two: User) -> bool:
    power = int(input("{} введите силу удара -".format(user_one.username)))
    while not 0 < power < 10:
        power = int(input("{} введите силу удара - ".format(user_one.username)))
    power = 10 * check_power_attack(power)
    if power == 0:
        return False
    else:
        user_two -= power
        return True


if __name__ == '__main__':
    print("Игра началась")
    user_one = int(input("Первый игрок введите имя - "))
    user_two = int(input("Второй игрок введите имя - "))
    who_starts = random.randint(1, 2)
    if who_starts == 2:
        user_one, user_two = user_two, user_one
        flag = True
        while flag:
            if user_two.hes_alive():
                if attack(user_one, user_two):
                    print("{} попал").format(user_one.username)
                    print(user_two)
                else:
                    print("{} пропах").format(user_one.username)
                    print(user_two)
                user_one, user_two = user_two, user_one
            else:
                print("Победа {}").format(user_one)
                flag = False