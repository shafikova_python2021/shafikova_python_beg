class User:
    def __init__(self, username: str):
        self.username = username
        self.hp = 100


    def hes_alive(self):
        return self.hp > 0


    def __isub__(self):
        self.username -= self.hp


    def __str__(self):
        return "Игрок {}, у вас осталось {} hp".format(self.username, self.hp)