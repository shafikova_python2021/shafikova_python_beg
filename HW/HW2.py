if __name__ == '__main__':
    num = int(input("Ввелите число от 0 до 100 - "))
    print("Число лежит в промежутке от 10 до 20")
    answer_one = (num > 10) and (num < 20)
    print(answer_one)
    print("Да" if answer_one else "Нет")
    if (num > 10) and (num < 20):
        print("Оно чётное?")
        answer_two = (num % 2) == 0
        print("Да" if answer_two else "Нет")
        if num % 2 == 0:
            num /= 2
        else:
            num *= 2
        print(num)
    else:
        num *= 2
    print(num)