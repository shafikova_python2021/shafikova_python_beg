def how_many_matches_to_the_upper_letter(string: str):
    sum = 0
    for elem in string:
        if 'A' <= elem <= 'Z':
            sum += 1
    return sum

if __name__ == '__main__':
    string = input('Введите слова, через запятую - ')
    print(how_many_matches_to_the_upper_letter(string))