if __name__ == '__main__':
    num = int(input("Введите целое число - "))
    if num < 0:
        print("error")
    else:
        sum = 0
        while num != 0:
            sum += num
            num -= 1
        print("Сумма предшествующих чисел равна =", sum)
