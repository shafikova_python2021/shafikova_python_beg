class RationalFraction:

    def __init__(self, numerator: int = 1, denominator: int = 1):
        if denominator == 0:
            raise Exception
        self.numerator = numerator
        self.denominator = denominator


    def __reduce__(self):
        limit = min(self.numerator, self.denominator)
        for j in reversed(range(2, limit + 1)):
            if (self.numerator % j == 0) and (self.denominator % j == 0):
                self.numerator = int(self.numerator / j)
                self.denominator = int(self.denominator / j)


    def __add__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=(self.numerator * other.denominator) + (other.numerator * self.denominator))
        answer.reduce()
        return answer


    def add(self, other):
        self.denominator = self.denominator * other.denominator
        self.numerator = self.numerator * other.denominator + other.numerator * self.denominator
        self.reduce()


    def __sub__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=(self.numerator * other.denominator) - (other.numerator * self.denominator))
        answer.reduce()
        return answer


    def sub(self, other):
        self.denominator = self.denominator * other.denominator
        self.numerator = self.numerator * other.denominator - other.numerator * self.denominator
        self.reduce()


    def __mul__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=(self.numerator * other.numerator))
        answer.reduce()
        return answer


    def mul(self, other):
        self.denominator = self.denominator * other.denominator
        self.numerator = self.numerator * other.numerator
        self.reduce()


    def __div__(self, other):
        answer = RationalFraction(numerator = self.numerator * other.denominator,
                                  denominator = other.numerator * self.denominator)
        answer.reduce()
        return answer


    def div(self, other):
        self.numerator = self.numerator * other.denominator
        self.denominator = other.numerator * self.denominator
        self.reduce()


    def __str__(self):
        return "RationalFraction {}/{}".format(self.numerator, self.denominator)


    def value(self):
        return self.numerator / self.denominator


    def __eq__(self, other):
        return self.numerator == other.numerator and self.denominator == other.denominator


    def number_part(self):
        return self.numerator // self.denominator