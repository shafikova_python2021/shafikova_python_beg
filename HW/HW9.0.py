class Vector2d:

    def __init__(self, x: int = 0, y: int = 0):
        self.x = x
        self.y = y


    def __add__(self, other):
        return Vector2d(x=(self.x + other.x), y=(self.y + other.y))


    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y


    def __sub__(self, other):
        return Vector2d(x=(self.x - other.x), y=(self.y - other.y))


    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y


    def __mul__(self, other):
        return Vector2d(x=(self.x * other.x), y=(self.y * other.y))


    def __imul__(self, other):
        self.x *= other.x
        self.y *= other.y


    def __str__(self):
        return "Vector(x = {}, y = {})". format(self.x, self.y)


    def __len__(self):
        return abs(self.x - self.y)


    def scalar_product(self, other):
        return self.__len__() * len(other)


    def __eq__(self, other):
        return self.x == other.x and self.y == other.y