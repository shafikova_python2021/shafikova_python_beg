def get_array() -> list[int]:
    count = int(input('Введите кол-во эл - '))
    lst = []
    for i in range(count):
        lst.append(int(input('Введите эл - ')))
    return lst


def sum_of_array(lst: list[int]) -> int:
    sum = 0
    for elem in lst:
        sum += elem
    return sum


def min_max_of_array(lst: list[int]) -> (int, int):
    if len(lst) > 0:
        min_of_array = lst[0]
        max_of_array = lst[0]
        for elem in lst:
            min_of_array = elem if elem < min_of_array else min_of_array
            max_of_array = elem if elem > max_of_array else max_of_array
    return min_of_array, max_of_array


def factorial(number: int) -> int:
    fact = 1
    for i in range(2, number + 1):
        fact *= i
    return fact * number


def factorials_of_lot_array(number: int) -> int:
    facts = set()
    for elem in lst:
        facts.add(factorial(elem))
    return facts


if __name__ == '__main__':
    lst = get_array()
    print(sum_of_array(lst))
    print(min_max_of_array(lst))
    print(factorials_of_lot_array(lst))